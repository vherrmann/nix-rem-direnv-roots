module Main where

import Control.Monad
import Data.ByteString.Lazy.Char8 qualified as L
import Data.Functor
import Data.Maybe
import Data.Text.Lazy (Text)
import Data.Text.Lazy qualified as T
import Data.Text.Lazy.Encoding qualified as T
import Data.Time qualified as DT
import Data.Time.Clock.POSIX qualified as DT
import Data.Traversable
import Options.Applicative (eitherReader)
import Options.Applicative qualified as O
import PyF
import System.Locale.Current (currentLocale)
import System.Posix qualified as P
import System.Process.Typed
import System.ProgressBar qualified as B

data Root = Root
  { systemPath :: Text
  , storePath :: Text
  , lastModified :: DT.UTCTime
  }
  deriving stock (Eq, Show)

getDirenvRoots :: IO [Root]
getDirenvRoots = do
  byteStr <- readProcessStdout_ (proc "nix-store" ["--gc", "--print-roots"])
  let text = T.decodeUtf8 byteStr
      rootPaths :: [(Text, Text)] =
        T.lines text
          <&> \line -> case T.splitOn " -> " line of
            [_] -> error $ "Output of nix-store has no ' -> '\n Line: " <> T.unpack line
            [systemPath, storePath] -> (systemPath, storePath)
            _ -> error $ "Output of nix-store has too many ' -> '\n Line: " <> T.unpack line
      direnvRootPaths = filter (\(systemPath, _) -> "/.direnv/" `T.isInfixOf` systemPath) rootPaths

  putStrLn "Collecting direnv roots:"
  pbar <- B.newProgressBar B.defStyle 2 (B.Progress 0 (length direnvRootPaths) ())
  direnvRoots :: [Root] <-
    for
      direnvRootPaths
      \(systemPath, storePath) -> do
        B.incProgress pbar 1
        lastModifiedEpoch <- P.modificationTime <$> P.getSymbolicLinkStatus (T.unpack systemPath)
        let lastModified = DT.posixSecondsToUTCTime (realToFrac lastModifiedEpoch)
        pure Root{..}

  pure direnvRoots

data Opts = Opts
  { olderThan :: DT.NominalDiffTime
  , dryRun :: Bool
  }

parseDiffTime :: O.ReadM DT.NominalDiffTime
parseDiffTime = eitherReader $ \s ->
  let n :: Int = read (init s)
   in case last s of
        's' -> Right $ fromIntegral n
        'h' -> Right $ fromIntegral n * 60
        'd' -> Right $ fromIntegral n * DT.nominalDay
        'm' -> Right $ fromIntegral n * DT.nominalDay * 30
        'y' -> Right $ fromIntegral n * DT.nominalDay * 365.25
        _ -> Left "Wrong time format: Has to be like 1s/1h/1d/1m/1y."

optsParser :: O.Parser Opts
optsParser =
  Opts
    <$> O.option
      parseDiffTime
      ( O.long "--delete-older-than"
          <> O.short 'o'
          <> O.value 0
          <> O.help "Remove only older gc roots"
      )
    <*> O.switch
      ( O.long "dry-run"
          <> O.short 'q'
          <> O.help "Don't remove the gc roots"
      )

main :: IO ()
main = do
  Opts{..} <- O.execParser optsParserInfo
  direnvRoots <- getDirenvRoots
  locale <- currentLocale
  when dryRun $ putStrLn "Dryrun: "

  currentTime <- DT.getCurrentTime
  let remBeforeTime = DT.addUTCTime (-olderThan) currentTime
  forM_ direnvRoots \(Root{..}) ->
    when (lastModified <= remBeforeTime) $ do
      let date = DT.formatTime locale "%Y-%m-%d" lastModified
      putStrLn [fmt| {systemPath} -> {storePath}: {date} |]

      unless dryRun $
        P.removeLink (T.unpack systemPath)
 where
  optsParserInfo =
    O.info
      (optsParser O.<**> O.helper)
      (O.fullDesc <> O.header "nix-rem-direnv-roots - remove old direnv gc roots")
